<?php

namespace App\Services\Paypal;

use Illuminate\Support\Facades\Config;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Session;
use Illuminate\Support\Facades\URL;
use Redirect;
// use Illuminate\Support\Facades\Input;
/** All Paypal Details class **/
use PayPal\Rest\ApiContext;
use PayPal\Auth\OAuthTokenCredential;
use PayPal\Api\Item;
use PayPal\Api\ItemList;
use PayPal\Api\Payer;
use PayPal\Api\RedirectUrls;
use PayPal\Api\ExecutePayment;
use PayPal\Api\Amount;
use PayPal\Api\Details;
use PayPal\Api\Payment;
use PayPal\Api\PaymentExecution;
use PayPal\Api\Transaction;

class PaypalService
{
    public function __construct()
    {
        /** PayPal api context **/
        $paypal_conf = Config::get('paypal');
        $this->_api_context = new ApiContext(new OAuthTokenCredential(
                $paypal_conf['client_id'],
                $paypal_conf['secret'])
        );
        $this->_api_context->setConfig($paypal_conf['settings']);
    }

    public function payWithpaypal($request)
    {
        $response = [];
        // $payer->setPaymentMethod('paypal');

        // $item_1 = new Item();
        // $item_1->setName('Item 1') /** item name **/
        //             ->setCurrency('INR')
        //             ->setQuantity(1)
        //             ->setPrice($request->get('amount')); /** unit price **/
        // $item_list = new ItemList();
        // $item_list->setItems(array($item_1));

        // $details = new Details();
        // $details->setShipping(1.2)
        //         ->setTax(1.3)
        //         ->setSubtotal(17.50);

        // $amount = new Amount();
        // $amount->setCurrency('INR')->setTotal($request->get('amount'));

        // $amount = $request->get('amount');
        // $transaction = new Transaction();
        // $transaction->setAmount($amount)->setDescription('Your transaction description');

        $payer = new Payer();
        $payer->setPaymentMethod("paypal");

        // ### Itemized information
        // (Optional) Lets you specify item wise
        // information

        $amount = $request->get('amount');
        $item1 = new Item();
        $item1->setName('Ground Coffee 40 oz')
            ->setCurrency('INR')
            ->setQuantity(1)
            ->setSku("123123") // Similar to `item_number` in Classic API
            ->setPrice($amount);
        // $item2 = new Item();
        // $item2->setName('Granola bars')
        //     ->setCurrency('INR')
        //     ->setQuantity(5)
        //     ->setSku("321321") // Similar to `item_number` in Classic API
        //     ->setPrice($request->get('amount'));

        $itemList = new ItemList();
        $itemList->setItems(array($item1));

        // ### Additional payment details
        // Use this optional field to set additional
        // payment information such as tax, shipping
        // charges etc.
        $shippingCharge = 1.2;
        $tax = 1.3;
        $details = new Details();
        $details->setShipping($shippingCharge)
            ->setTax($tax)
            ->setSubtotal($amount);

        // ### Amount
        // Lets you specify a payment amount.
        // You can also specify additional details
        // such as shipping, tax.
        $total = $amount + $shippingCharge + $tax;
        $amount = new Amount();
        $amount->setCurrency("INR")
            ->setTotal($total)
            ->setDetails($details);

        // ### Transaction
        // A transaction defines the contract of a
        // payment - what is the payment for and who
        // is fulfilling it. 
        $transaction = new Transaction();
        $transaction->setAmount($amount)
            ->setItemList($itemList)
            ->setDescription("Payment description")
            ->setInvoiceNumber(uniqid());

        // ### Redirect urls
        // Set the urls that the buyer must be redirected to after 
        // payment approval/ cancellation.

        $url = URL::route('status');
        $redirect_urls = new RedirectUrls();
        $redirect_urls->setReturnUrl($url.'?success=true') /** Specify return URL **/
                      ->setCancelUrl($url.'?success=false');
                      
        // $payment = new Payment();
        // $payment->setIntent('sale')
        //             ->setPayer($payer)
        //             ->setRedirectUrls($redirect_urls)
        //             ->setTransactions(array($transaction));


        // ### Payment
        // A Payment Resource; create one using
        // the above types and intent set to 'sale'
        $payment = new Payment();
        $payment->setIntent("sale")
                ->setPayer($payer)
                ->setRedirectUrls($redirect_urls)
                ->setTransactions(array($transaction));

        
        // dd($payer, $redirect_urls, $payment);
        try {
            $payment->create($this->_api_context);
        }
        catch (\PayPal\Exception\PayPalConnectionException $ex) {
            // echo $ex->getCode(); // Prints the Error Code
            // echo $ex->getData(); // Prints the detailed error message 
                $response['success'] = false;

                if (Config::get('app.debug')) {
                    Session::put('error', 'Connection timeout');
                } else {
                    Session::put('error', 'Some error occur, sorry for inconvenient');
                }
                return $response;
        }
        foreach ($payment->getLinks() as $link) {
            if ($link->getRel() == 'approval_url') {
                $redirect_url = $link->getHref();
                break;
            }
        }
            
        /** add payment ID to session **/
        Session::put('paypal_payment_id', $payment->getId());
        if (isset($redirect_url)) {
            /** redirect to paypal **/
            $response['redirect_paypal_url'] = $redirect_url;
            $response['success'] = true;
            return $response;
        }
        Session::put('error', 'Unknown error occurred');
        return $response['success'] = false;
    }

    public function getPaymentStatus()
    {
        /** Get the payment ID before session clear **/
        $payment_id = Session::get('paypal_payment_id');
        /** clear the session payment ID **/
        Session::forget('paypal_payment_id');
        if (empty(request()->get('PayerID')) || empty(request()->get('token'))) {
            Session::put('error', 'Payment failed');
            return $response['success'] = false;
        }
        $payment = Payment::get($payment_id, $this->_api_context);
        $execution = new PaymentExecution();
        $execution->setPayerId(request()->get('PayerID'));

        /**Execute the payment **/
        $result = $payment->execute($execution, $this->_api_context);
        if ($result->getState() == 'approved') {
            Log::channel('payments')->info($result);
            Session::put('success', 'Payment success');
            return $response['success'] = true;
        }
        Session::put('error', 'Payment failed');
        return $response['success'] = false;
    }

}