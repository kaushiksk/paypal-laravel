<?php

namespace App\Services\UserRole;

use Illuminate\Support\Facades\DB;

class UserRoleService
{
    protected $userRole;
    public function __construct()
    {
        $this->userRole = DB::table('user_role');
    }

     /**
     * @return object
     */
    public function create($userRoleData = [])
    {
        return $this->userRole->insert($userRoleData);
    }
}