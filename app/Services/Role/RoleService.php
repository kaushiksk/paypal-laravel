<?php

namespace App\Services\Role;

use App\Model\Role;

class RoleService
{
    protected $role;
    public function __construct(Role $role)
    {
        $this->role = $role;
    }

     /**
     * @return object
     */
    public function index()
    {
        return $this->role->all();
    }

    /**
     * @return object
     */
    public function getRoleBySlug($role)
    {
        return $this->role->where('slug', $role)->first();
    }
}