<?php

namespace App\Services\User;

use App\User;
use Illuminate\Support\Facades\Hash;

class UserService
{
    protected $user;
    public function __construct(User $user)
    {
        $this->user = $user;
    }

     /**
     * @return object
     */
    public function index()
    {
        return $this->user->all();
    }

    /**
     * @return object
     */
    public function create($user = [])
    {
        return $this->user->create($user);
    }

    /**
     * 
     */
    protected function getUserData()
    {
        return [
            'name' => request()->name(),
            'email' => request()->email(),
            'password' => Hash::make('secret')
        ];
        
    }
}