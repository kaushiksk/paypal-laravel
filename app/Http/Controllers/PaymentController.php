<?php

namespace App\Http\Controllers;

use App\Services\Paypal\PaypalService;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Redirect;

class PaymentController extends Controller
{
    //
    protected $paypalService;

    public function __construct(PaypalService $paypalService)
    {
        $this->paypalService = $paypalService;
    }


    public function index()
    {
        return view('payment.paypal');
    }

    public function payWithpaypal(Request $request)
    {
        $response = $this->paypalService->payWithpaypal($request);
        if($response['success']) 
            return redirect()->away($response['redirect_paypal_url']);
        
        return redirect()->route('paypal-amount-box');
    }

    public function getPaymentStatus()
    {
        $response = $this->paypalService->getPaymentStatus();
        if($response['success']) 
            return redirect()->away($response['redirect_paypal_url']);
    
        return redirect()->route('home');
    }
    
    public function callback()
    {
        $response = $this->paypalService->callback();
        if($response['success']) 
            return redirect()->away($response['redirect_paypal_url']);
    
        return redirect()->route('home');
    }
    
    public function customRedirecUrl()
    {
        Log::channel('payments')->info('Custom redirect url');
    }
}
