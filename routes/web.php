<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');
Route::get('/add-payment', 'PaymentController@index')->name('paypal-amount-box');
Route::post('/payment/add-funds/paypal', 'PaymentController@payWithpaypal')->name('add-paypal-payment');
Route::get('/status', 'PaymentController@getPaymentStatus')->name('status');
Route::get('/callback', 'PaymentController@callback')->name('paypal-callback');
Route::get('/custom-redirect-url', 'PaymentController@customRedirecUrl')->name('custom-redirect');

