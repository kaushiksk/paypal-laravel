<?php

use App\Model\Role;
use Illuminate\Database\Seeder;

class RoleSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $roles = [
            ['name' => 'Super-Admin', 'slug' => 'super-admin'],
            ['name' => 'User', 'slug' => 'user'],
        ];
        Role::Insert($roles);
    }
}
