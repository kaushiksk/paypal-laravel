<?php

use App\Services\Role\RoleService;
use App\Services\User\UserService;
use App\Services\UserRole\UserRoleService;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\Hash;

class UserSeeder extends Seeder
{
    protected $roleService, $userService, $userRoleService;
    public function __construct(RoleService $roleService, UserService $userService, UserRoleService $userRoleService)
    {
        $this->roleService = $roleService;
        $this->userService = $userService;
        $this->userRoleService = $userRoleService;
    }
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $user = 
        [
            'name' => 'Abc',
            'email' => 'abc@user.com',
            'password' => Hash::make('12345678'),
        ];
        $userId = $this->userService->create($user)->id;
        $roleId = $this->roleService->getRoleBySlug('user')->id;
        $this->userRoleService->create(
            [
                'role_id' => $roleId,
                'user_id' => $userId
            ]
        );
    }
}
