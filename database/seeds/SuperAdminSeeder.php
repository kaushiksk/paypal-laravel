<?php

use App\Model\Role;
use App\Services\Role\RoleService;
use App\Services\User\UserService;
use App\Services\UserRole\UserRoleService;
use App\User;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;

class SuperAdminSeeder extends Seeder
{
    protected $roleService, $userService, $userRoleService;
    public function __construct(RoleService $roleService, UserService $userService, UserRoleService $userRoleService)
    {
        $this->roleService = $roleService;
        $this->userService = $userService;
        $this->userRoleService = $userRoleService;
    }

    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $superAdmin = 
        [
            'name' => 'SuperAdmin',
            'email' => 'superadmin@admin.com',
            'password' => Hash::make('secret'),
        ];
        $userId = $this->userService->create($superAdmin)->id;
        $roleId = $this->roleService->getRoleBySlug('super-admin')->id;
        $this->userRoleService->create(
            [
                'role_id' => $roleId,
                'user_id' => $userId
            ]
        );
    }
}
